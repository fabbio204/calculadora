import 'dart:math';

import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

class DinheiroInputFormatter extends TextInputFormatter {
  num _newNum = 0;
  String _newString = '';

  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    if (newValue.selection.baseOffset == 0) {
      return newValue;
    }

    String newText = newValue.text.replaceAll(RegExp('[^0-9]'), '');

    _formatter(newText);

    return newValue.copyWith(
      text: _newString,
      selection: TextSelection.collapsed(offset: _newString.length),
    );
  }

  void _formatter(String newText) {
    final formatter = NumberFormat.simpleCurrency(locale: "pt_Br");

    _newNum = num.tryParse(newText) ?? 0;

    if (formatter.decimalDigits! > 0) {
      _newNum /= pow(10, formatter.decimalDigits!);
    }
    _newString = formatter.format(_newNum).trim();
  }
}
