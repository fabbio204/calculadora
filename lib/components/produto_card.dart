import 'package:calculadora_produtos/formatters/dinheiro_input_formatter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ProdutoCard extends StatefulWidget {
  const ProdutoCard({Key? key}) : super(key: key);

  @override
  State<ProdutoCard> createState() => _ProdutoCardState();
}

class _ProdutoCardState extends State<ProdutoCard> {
  UnidadeMedida _unidadeMedida = UnidadeMedida.g;
  String _preco = '';
  String _peso = '';
  String _precoPorKg = '';

  _calcularPreco() {
    if (_preco.isEmpty || _peso.isEmpty) {
      setState(() {
        _precoPorKg = '';
      });
      return;
    }

    String precoConvertido = _preco
        .replaceAll(RegExp('[R][\$][\\s]'), '')
        .replaceAll(RegExp('[.]'), '')
        .replaceAll(RegExp('[,]'), '.');
    String pesoConvertido = _peso.replaceAll('.', '').replaceAll(',', '.');

    double preco = double.parse(precoConvertido);
    double peso = double.parse(pesoConvertido);

    if (peso == 0 || preco == 0) {
      setState(() {
        _precoPorKg = '';
      });
      return;
    }

    int conversaoGrandeza = (_unidadeMedida == UnidadeMedida.kg ||
            _unidadeMedida == UnidadeMedida.l)
        ? 1
        : 1000;

    double total = preco / peso * conversaoGrandeza;

    setState(() {
      _precoPorKg = 'R\$ ${total.toStringAsFixed(2).replaceAll('.', ',')}';
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(2.0),
      child: Card(
        elevation: 5.0,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Expanded(
                    flex: 1,
                    child: Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: DropdownButtonFormField<UnidadeMedida>(
                        items: _UnidadeMedida.opcoes
                            .map(
                              (item) => DropdownMenuItem<UnidadeMedida>(
                                value: item.key,
                                child: Text(item.value),
                              ),
                            )
                            .toList(),
                        onChanged: (UnidadeMedida? valorSelecionado) {
                          if (valorSelecionado != null) {
                            setState(() {
                              _unidadeMedida = valorSelecionado;
                              _calcularPreco();
                            });
                          }
                        },
                        value: _unidadeMedida,
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: TextFormField(
                        keyboardType: TextInputType.number,
                        onChanged: (String value) {
                          _peso = value;
                          _calcularPreco();
                        },
                        inputFormatters: [
                          FilteringTextInputFormatter.allow(RegExp(r'[0-9,]'))
                        ],
                        textInputAction: TextInputAction.next,
                        decoration: InputDecoration(
                          labelText: _UnidadeMedida.getTitulo(_unidadeMedida),
                          border: const OutlineInputBorder(),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Campo obrigatório';
                          }
                          return null;
                        },
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: TextFormField(
                        keyboardType: TextInputType.number,
                        textInputAction: TextInputAction.next,
                        inputFormatters: [
                          FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                          DinheiroInputFormatter(),
                        ],
                        onChanged: (String value) {
                          _preco = value;
                          _calcularPreco();
                        },
                        decoration: const InputDecoration(
                          labelText: 'Preço (R\$)',
                          border: OutlineInputBorder(),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Campo obrigatório';
                          }
                          return null;
                        },
                      ),
                    ),
                  ),
                ],
              ),
              if (_precoPorKg.isNotEmpty)
                Text.rich(
                  TextSpan(
                    style: TextStyle(fontSize: 10, color: Colors.grey[700]),
                    children: [
                      TextSpan(
                          text:
                              'Preço por ${_UnidadeMedida.getNomeGrandezaMaior(_unidadeMedida)}: '),
                      TextSpan(text: _precoPorKg)
                    ],
                  ),
                )
            ],
          ),
        ),
      ),
    );
  }
}

enum UnidadeMedida { g, kg, ml, l }

class _UnidadeMedida {
  static String getNome(UnidadeMedida unidade) {
    switch (unidade) {
      case UnidadeMedida.kg:
        return 'kg';

      case UnidadeMedida.g:
        return 'g';

      case UnidadeMedida.ml:
        return 'ml';

      case UnidadeMedida.l:
        return 'l';
    }
  }

  static String getNomeGrandezaMaior(UnidadeMedida unidade) {
    switch (unidade) {
      case UnidadeMedida.kg:
      case UnidadeMedida.g:
        return 'Quilo';

      case UnidadeMedida.ml:
      case UnidadeMedida.l:
        return 'Litro';
    }
  }

  static String getTitulo(UnidadeMedida unidade) {
    switch (unidade) {
      case UnidadeMedida.g:
        return 'Peso (gramas)';

      case UnidadeMedida.kg:
        return 'Peso (quilo)';

      case UnidadeMedida.ml:
        return 'Volume (ml)';

      case UnidadeMedida.l:
        return 'Volume (litro)';
    }
  }

  static List<MapEntry<UnidadeMedida, String>> get opcoes => [
        MapEntry(UnidadeMedida.g, getNome(UnidadeMedida.g)),
        MapEntry(UnidadeMedida.kg, getNome(UnidadeMedida.kg)),
        MapEntry(UnidadeMedida.ml, getNome(UnidadeMedida.ml)),
        MapEntry(UnidadeMedida.l, getNome(UnidadeMedida.l)),
      ];
}
