import 'package:calculadora_produtos/components/produto_card.dart';
import 'package:flutter/material.dart';

class CalcularPrecoPage extends StatefulWidget {
  const CalcularPrecoPage({Key? key}) : super(key: key);

  @override
  State<CalcularPrecoPage> createState() => _CalcularPrecoPageState();
}

class _CalcularPrecoPageState extends State<CalcularPrecoPage> {
  final _formKey = GlobalKey<FormState>();

  List<ProdutoCard> produtos = [
    const ProdutoCard(),
    const ProdutoCard(),
  ];

  _adicionar() {
    setState(() {
      produtos.add(const ProdutoCard());
    });
  }

  _limpar() {
    showDialog(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        title: const Text('Limpar dados'),
        content: const Text('Tem certeza que deseja limpar todos os dados?'),
        actions: [
          TextButton(
            onPressed: () => Navigator.pop(context, 'Cancelar'),
            child: const Text('Cancelar'),
          ),
          TextButton(
            onPressed: () {
              Navigator.pop(context, 'OK');
              setState(() {
                produtos.clear();
                produtos.addAll([
                  ProdutoCard(key: UniqueKey()),
                  ProdutoCard(key: UniqueKey()),
                ]);
              });
            },
            child: const Text('OK'),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: produtos,
              ),
            ),
          ),
          Row(
            children: [
              Expanded(
                  child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(primary: Colors.red),
                  onPressed: _limpar,
                  child: const Text('Limpar'),
                ),
              )),
              Expanded(
                  child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: ElevatedButton(
                  onPressed: _adicionar,
                  child: const Text('Adicionar'),
                ),
              ))
            ],
          )
        ],
      ),
    );
  }
}
