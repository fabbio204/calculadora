import 'package:flutter/material.dart';

import 'package:calculadora_produtos/pages/calcular_preco_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Calculadora de Preços',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Calculadora de Preços'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  OpcoesDrawer itemDrawerAtual = OpcoesDrawer.calcularPreco;

  Widget telaAtual(OpcoesDrawer item) {
    switch (item) {
      case OpcoesDrawer.calcularPreco:
        return const CalcularPrecoPage();

      default:
        return Container();
    }
  }

  void _fecharDrawer() {
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: telaAtual(itemDrawerAtual),
      drawer: Drawer(
        elevation: 5,
        child: ListView(
          children: [
            ListTile(
              leading: const Text('Quantidade x Preço'),
              onTap: () {
                setState(() {
                  itemDrawerAtual = OpcoesDrawer.calcularPreco;
                });
                _fecharDrawer();
              },
            )
          ],
        ),
      ),
    );
  }
}

enum OpcoesDrawer { calcularPreco }
